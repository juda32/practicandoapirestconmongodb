package mis.pruebas.api.modelos;

import org.springframework.data.annotation.Id;

public class Cuenta {

    @Id
    public String numero;
    public String moneda;
    public Double saldo;
    public String tipo;
    public String estado;
    public String oficina;

}
