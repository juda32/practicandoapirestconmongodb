package mis.pruebas.api.controlador;

import mis.pruebas.api.modelos.Cliente;
import mis.pruebas.api.servicios.ObjetoNoEncontrado;
import mis.pruebas.api.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    // /api/v1/cliente/1234
    // /api/v1/cliente/1234/cuentas -->{} o {'3333444','56449555'}


    @Autowired
    ServicioCliente servicioCliente;

    public static class ResumenCliente{
        public String documento;
        public String nombre;

        public static ResumenCliente of(Cliente x){
            final ResumenCliente resumenCliente = new ResumenCliente();
            resumenCliente.documento =x.documento;
            resumenCliente.nombre =x.nombre;
            return resumenCliente;
        }
    }

    //GET http://localhost:9000/api/v1/clientes -> List<Cliente> obtenerClientes()
    @GetMapping
    public CollectionModel<EntityModel<Cliente>> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad){
        try{
            final var clientes = this.servicioCliente.obtenerClientes(pagina -1 , cantidad);

            final var resumenClientes = clientes.stream()
                    .map(x -> ResumenCliente.of(x)).collect(Collectors.toList());

            final var emClientes= clientes.stream().map
                    (x -> EntityModel.of(x).add(
                    linkTo(methodOn(this.getClass()).obtenerUnCliente(x.documento))
                            .withSelfRel().withTitle("Este Cliente"),
                    linkTo(methodOn(ControladorCuentasCliente.class).obtenerCuentasCliente(x.documento))
                            .withRel("cuentas").withTitle("Cuentas del cliente")
                    )
            ).collect(Collectors.toList());
            return CollectionModel.of(emClientes).add(
                    linkTo(methodOn(this.getClass()).obtenerClientes(1,100)).withSelfRel().withTitle("Todos los Clientes")
            );
        } catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }


    }
    //link (rel , url)

    //POST http://localhost:9000/api/v1/clientes + DATOS -> agregarCliente(Cliente)
    //200 OK, LOCATION : http://localhost:9000/api/v1/clientes/109343498
    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente){
        this.servicioCliente.insertarClienteNuevo(cliente);

        // obtenerUnCliente(documento) --> http://localhost:9000/api/v1/clientes/109343498
        //link.url --> Location
        final var enlaceEsteDocumento = linkTo(methodOn(ControladorCliente.class).obtenerUnCliente(cliente.documento)).toUri();

        return ResponseEntity.ok().location(enlaceEsteDocumento).build();

    }

    //Link
    //EntityModel<Cliente>
    //CollectionModel<EntityModel<Cliente>>
    //GET http://localhost:9000/api/v1/clientes/{documento} ->obtenerUnCliente(documento)
    //GET http://localhost:9000/api/v1/clientes/12345678 ->obtenerUnCliente("12345678")
    @GetMapping("/{documento}")
    public EntityModel<Cliente> obtenerUnCliente(@PathVariable  String documento) {
        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerUnCliente(documento)).withSelfRel();
            final Link cuentas = linkTo(methodOn(ControladorCuentasCliente.class).obtenerCuentasCliente(documento))
                    .withRel("cuentas").withTitle("Cuentas de este Cliente");
            final Link clientes = linkTo(methodOn(this.getClass()).obtenerClientes(1, 100))
                    .withRel("todosclientes").withTitle("Lista todos los clientes");
            final var cliente =this.servicioCliente.obtenerCliente(documento);
            return EntityModel.of(cliente).add(self, cuentas, clientes);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    //PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS->reemplacarunCliente(documento, Datos)
    //PUT http://localhost:9000/api/v1/clientes/12345678 ->reemplacarunCliente("12345678")
    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente){
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.guardarCliente(cliente);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    //PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS->emparcharunCliente(documento, Datos)
    //PUT http://localhost:9000/api/v1/clientes/12345678 ->emparcharrunCliente("12345678")
    @PatchMapping("/{documento}")
    public void emparcharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente){
        try {
        cliente.documento = nroDocumento;
        this.servicioCliente.emparcharCliente(cliente);
        }catch (ObjetoNoEncontrado x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        }
    }

    //DELETE http://localhost:9000/api/v1/clientes/{documento} + DATOS->eliminarunCliente(documento, Datos)
    //DELETE http://localhost:9000/api/v1/clientes/12345678 ->eeliminarrunCliente("12345678")
    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarUnCliente(@PathVariable String documento){
        try {
            this.servicioCliente.borrarCliente(documento);
        } catch (ObjetoNoEncontrado x) {
        }
    }
}
