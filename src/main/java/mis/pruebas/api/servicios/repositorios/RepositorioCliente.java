package mis.pruebas.api.servicios.repositorios;

import mis.pruebas.api.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepositorioCliente extends MongoRepository<Cliente, String> {

    public Optional<Cliente> findByDocumento(String documento);

    @Query(value = "{nombre : ?0 , edad: ?1}")
    public List<Cliente> obtenerClientePorNombreEdad(String nombre, String edad);
    public void deleteByDocumento(String documento);
}
