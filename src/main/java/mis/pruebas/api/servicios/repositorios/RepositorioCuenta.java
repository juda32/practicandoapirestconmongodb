package mis.pruebas.api.servicios.repositorios;

import mis.pruebas.api.modelos.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCuenta extends MongoRepository<Cuenta,String> {
}
