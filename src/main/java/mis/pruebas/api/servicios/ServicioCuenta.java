package mis.pruebas.api.servicios;

import mis.pruebas.api.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas(int pagina, int cantidad);
    //CREATE
    public void insertarCuentaNueva(Cuenta cuenta);

    //READ
    public Cuenta obtenerCuenta(String cuenta);

    //update (solamente modificar no crear)
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta cuenta);

    public void borrarCuenta(String numero);

    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta);

    public void eliminarCuentaCliente(String documento, String numeroCuenta);

}
