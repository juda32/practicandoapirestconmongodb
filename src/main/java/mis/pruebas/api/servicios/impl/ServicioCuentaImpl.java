package mis.pruebas.api.servicios.impl;

import mis.pruebas.api.modelos.Cliente;
import mis.pruebas.api.modelos.Cuenta;
import mis.pruebas.api.servicios.ObjetoNoEncontrado;
import mis.pruebas.api.servicios.ServicioCliente;
import mis.pruebas.api.servicios.ServicioCuenta;
import mis.pruebas.api.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    @Autowired
    RepositorioCuenta repositorioCuenta;
    @Autowired
    ServicioCliente servicioCliente;


    @Override
    public List<Cuenta> obtenerCuentas(int pagina, int cantidad) {
        return this.repositorioCuenta.findAll();
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.repositorioCuenta.insert(cuenta);

    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        final var quizasCuenta =this.repositorioCuenta.findById(numero);
        if(!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta numero" + numero);
        return quizasCuenta.get();
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if (!this.repositorioCuenta.existsById(cuenta.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta numero" + cuenta.numero);
        this.repositorioCuenta.save(cuenta);

    }

    @Override
    public void emparcharCuenta(Cuenta cuenta) {
        throw new UnsupportedOperationException("No implementado");
    }

    @Override
    public void borrarCuenta(String numero) {
        throw new UnsupportedOperationException("No implementado");

    }

    @Override
    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta) {
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta numero" + numeroCuenta);

        final var cliente = this.servicioCliente.obtenerCliente(documento);

        if (!cliente.codigosCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("El cliente no tiene cuenta asociada" + numeroCuenta);

        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if (!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta numero" + numeroCuenta);

        return quizasCuenta.get();
    }

    @Override
    public void eliminarCuentaCliente(String documento, String numeroCuenta) {
        final var cliente = this.servicioCliente.obtenerCliente(documento);
        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if (!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta" + numeroCuenta);
        if (!cliente.codigosCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("El cliente no tiene esta cuenta asociada" + numeroCuenta);

        final var cuenta =quizasCuenta.get();
            cuenta.estado = "INACTIVA";
            cliente.codigosCuentas.remove(numeroCuenta);
            guardarCuenta(cuenta);
            this.servicioCliente.guardarCliente(cliente);
        }
}
