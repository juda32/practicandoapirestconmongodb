package mis.pruebas.api.servicios;

public class ObjetoNoEncontrado extends RuntimeException{
    public ObjetoNoEncontrado(String message){
        super(message);
    }
}

